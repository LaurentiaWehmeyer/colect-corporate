export const state = () => ({
  settings: {
    img: {},
    header: '',
    subheader: '',
    cta: {},
  },
})

export const mutations = {
  setSettings(state, data) {
    state.settings.img = data.img
    state.settings.header = data.header
    state.settings.subheader = data.subheader
    ;[state.settings.cta] = data.cta
  },
}

export const actions = {
  loadHomePageThings({ commit }, context) {
    return this.$storyapi
      .get('cdn/stories/home', {
        version: context.version,
      })
      .then((res) => {
        commit('setSettings', res.data.story.content.body[0])
      })
  },
}
